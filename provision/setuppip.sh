#!/usr/bin/env bash

echo "Installing pip..."
sudo apt-get update
sudo apt-get -y install python-pip 
sudo curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
python get-pip.py

echo "Installing Flask..."
pip install Flask