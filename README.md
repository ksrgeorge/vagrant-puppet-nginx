Automation Logic Test

The solution to the test consists of a single Vagrantfile and various configuration files for the setup of 
three VMs commuicating with Nginx server, with the use of the configuration management tool Puppet.

· Create a Vagrantfile that creates a single machine using this box: https://vagrantcloud.com/puppetlabs/boxes/ubuntu-14.04-64-nocm
and installs the latest released version of your chosen configuration management tool.

First we have to download and install the latest version of Vagrant from HashiCorp.
Then we create a directory for the project and navigate into that directory:

	$mkdir Vagrant-puppet
	$cd Vagrant-puppet
	
In that directory we create three more subdirectories that we will need in the next steps. These are App, provision and puppet.
Inside the puppet folder we create the two required sub directories, manifests and modules. Inside the modules directory we create 
directories for the main three modules we are going to use: nginx, app and sudoers all with their own manifest folders respectively.
Finally for the modules nginx and sudoers we create an additional subdirectory called files, where we are going to store the dependencies files 
for the two modules.
	
Using Vagrant we download and initialize the specified box of Ubuntu 14.04-64-nocm.
	
	$vagrant init puppetlabs/ubuntu-14.04-64-nocm

This creates a Vagrantfile that we can edit so it looks like that:

host_port = 8080
servers=[
  {
    :hostname => "webfirst",
    :ip => "192.168.10.10",
    :role => "appserver"
  },
  {
    :hostname => "websecond",
    :ip => "192.168.10.11",
    :role => "appserver"
  },
  {
    :hostname => "lb",
    :ip => "192.168.10.12",
    :role => "loadbalancer",
  }
]

webs = []
servers.each do |host|
  if host[:hostname] != "lb"
    webs << host[:ip]
  end
end

Vagrant.configure("2") do |config|
  servers.each do |machine|
    config.vm.define machine[:hostname] do |node|
      node.vm.box = "puppetlabs/ubuntu-14.04-64-nocm"
      node.vm.network "private_network", ip: machine[:ip]
      node.vm.hostname = machine[:hostname]
	  config.vm.provision "shell" do |s|
        s.path = "provision/setup.sh"
      end
      config.vm.provision :puppet do |puppet|
        puppet.manifests_path = 'puppet/manifests'
        puppet.module_path = 'puppet/modules'
        puppet.manifest_file = 'init.pp'
      end
      if machine[:role] == "loadbalancer" then
         node.vm.network :forwarded_port, guest: 80, host: host_port
		 config.vm.provision :shell, path: "test_port.sh"
      end
    end
  end
end

At first we declare the host_port as 8000. Then we assign in the list called servers the dictionaries for each of the machines we are going to use.
Each of the dictionaries carries facts for the hostname, ip and role for each machine.
Afterwards we start the configuration of the boxes beginning with Vagrant.configure("2") do |config|. Because we want Vagrant to create 3 VMs we call configureation for each of the three machines.

The configuration management I chose is the open source Puppet. The reasons I chose this CM tool are the very detailed documentation, the existence of librarian-puppet tool and the great library of ready to install modules.

To install puppet we use vagrant's shell provisioning with the help of an external script file setup.sh located in the provision file.

	#!/usr/bin/env bash

	echo "Provisioning virtual machine..."



	echo "Installing Puppet"
	sudo apt-get update
	sudo apt-get -y install puppet


Next using puppet provision we declare where the path for the manifests and the modules will be.

· Run a simple test using Vagrant's shell provisioner to ensure that nginx is listening on port 80

The last step is to port forward the host_port to the guest port 80 through vm provision and to check if the vm is listening there with the help of th test_port.sh file.

· Install the nginx webserver via configuration management.

The first module is Nginx. With the help of the init.pp manifest we install the Nginx server and we ensure that it's service is running. So the init.pp looks like:

	class nginx {

	  package { 'nginx':
		ensure => 'present',
		require => Exec['apt-get update'],
	  }

	  service { 'nginx':
		ensure => running,
		require => Package['nginx'],

	  }
	}


· Again, using configuration management, update contents of /etc/sudoers file so that Vagrant user can sudo without a password and that anyone in the admin group can sudo with a password.

We move now to the second module, sudoers. The manifest file for this module updates in every machine the sudoers file in /etc/sudoers so that anyone in the admin group can sudo without a password:

	# User privilege specification
	root    ALL=(ALL) ALL
	# add vagrant user
	vagrant ALL=(ALL) NOPASSWD:ALL
	# users in admin group can sudo with a password
	%admin  ALL=(ALL) ALL

· Make the solution idempotent so that re-running the provisioning step will not restart nginx unless changes have been made
Puppet does that by itself. While running the provision again nginx does not restart.

· Create a simple "Hello World" web application in your favourite language of choice.

The web application is a simple hello world application in Python with the help of Flask microframework for Python.
To use it in the vm we have to install some modules from puppetforge. 
Enter librarian-puppet. We install librarian puppet and we init it in the directory puppet. I edited the generated Puppetfile according to the documentation so that the librarian installs the required modules
in the foldedr modules. The latest module to install is the stankevich-python module that installs in the vm the gunicorn server that will serve as the appserver for each the machines.

· Ensure your web application is available via the nginx instance.
· Extend the Vagrantfile to deploy this webapp to two additional vagrant machines and then configure the nginx to load balance between them.

The current configuration allows us to have the nginx server communicating with the other two vms, returning the output hello world. Just do vagrant up to begin the boxes. Next we vagrant ssh in the loadbalancer machine
and we test if the nginx communicates with the two additional machines:

	$curl ipmachine:8080
	
We can see that it returns hello world.
