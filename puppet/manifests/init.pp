#Run apt-get update;
exec { 'apt-get update':
 path => '/usr/bin',
}

include sudoers

node "lb" {
   include nginx
}

node "webfirst", "websecond" {
   include app
}