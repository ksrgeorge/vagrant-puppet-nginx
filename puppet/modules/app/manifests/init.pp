
class app {
  package { ['git']:
    ensure => latest
  }
  group {'application':
  	name => "application",
	ensure => "present"
  }

  user { 'application':
    name       => "application",
    uid        => "532",
    shell      => "/bin/bash",
    home       => "/home/application",
    managehome => true ,
    groups     => ['application', 'www-data'],
    require    => [ Group['application'] ]
  }

  file { '/home/application/src':
    ensure  => 'directory',
    owner   => 'application',
    group   => 'application',
    require => [ User['application'], Group['application'] ]
  }
  

  vcsrepo { '/home/application/src/application':
    ensure   => latest,
    user     => 'application',
    group    => 'application',
    owner    => 'application',
    provider => 'git',
    source   => 'https://ksrgeorge@bitbucket.org/ksrgeorge/hello_world.git',
    force    => true,
    require  => [ Package['git'],
		          User['application'],
		          Group['application'],
		          File['/home/application/src']
				 ]
  }

  class { 'python' :
    version    => 'system',
    pip        => 'present',
    dev        => 'latest',
    virtualenv => 'latest',
    gunicorn   => 'latest',
  }
 
 
  python::virtualenv { '/home/application/venv' :
    ensure       => present,
    version      => 'system',
    systempkgs   => true,
    requirements => '/home/application/src/application/requirements.txt',
    venv_dir     => '/home/application/venv',
    owner        => 'application',
    group        => 'application',
    cwd          => '/home/application/src/application',
  }
 
  python::gunicorn { 'hello' :
    ensure      => present,
    virtualenv  => '/home/application/venv',
    mode        => 'wsgi',
    dir         => '/home/application/src/application',
    bind        => '0.0.0.0:8080',
    environment => 'prod',
    appmodule   => 'hello_world:app',
    timeout     => 30,
    template    => 'python/gunicorn.erb',
  }
} 
