class nginx {
  
  package { 'nginx':
    ensure => 'present',
    require => Exec['apt-get update'],
  }

  service { 'nginx':
    ensure => running,
    require => Package['nginx'],

  }

  file {'example.com':
    path => '/etc/nginx/sites-available/example.com',
    ensure => file,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/example.com',
  }

  file { 'default-nginx-disable':
    path => '/etc/nginx/sites-enabled/default',
    ensure => absent,
    require => Package['nginx'],
  }
}


